# distributed-system

This is shell project for creating highthrought put system with the use of SpringFramework microservice & Kafka for events

## Deploying on local

**run the following command to Initialize Kafka**

`docker-compose -f docker/kafka-zoo-singlenode.yml up`

**run the following command to initilize mongoDB and mongoDB express**

`docker-compose -f docker/mongodb-express.yml up`

**To run the spring boot application**

`cd springboot-app1`

`./gradlew bootRun`

**To Build & Push the docker image to AWS repo the springboot application**


`./gradlew clean build`

If ECR is not yet created run the following command

`./gradlew createECR -PimageRepo=<awsAccountId>.dkr.ecr.<region>.amazonaws.com`

`./gradlew dockerPush -PimageRepo=<awsAccountId>.dkr.ecr.<region>.amazonaws.com`


**To publishMessage to Topic**

`http://localhost:8080/trigger/{yourMessage}`


# Running on Kubernetes

Requirements:
 helm plugin
## Running Zookeeper on Kubernetes 

Documentation: https://github.com/bitnami/charts/tree/master/bitnami/zookeeper

`helm repo add bitnami https://charts.bitnami.com/bitnami`

`helm install zk bitnami/zookeeper`


## Running Kafka 

Documentation: https://github.com/bitnami/charts/tree/master/bitnami/kafka

`helm install kf -f kf_helm_val.yml bitnami/kafka`
